/* ==========================================================================
   Main JavaScript File
   ========================================================================== */

// Drupal doesn't like registering $ as jQuery, so forcing it here.
$ = jQuery;

///////// FastClick (removes delay on older mobile touch events) ///////
$(function() {
    FastClick.attach(document.body);
});
//Waypoints
//Waypoints Stickys
(function() {
  'use strict'

  var $ = window.jQuery
  var Waypoint = window.Waypoint

  /* http://imakewebthings.com/waypoints/shortcuts/sticky-elements */
  function Sticky(options) {
    this.options = $.extend({}, Waypoint.defaults, Sticky.defaults, options)
    this.element = this.options.element
    this.$element = $(this.element)
    this.createWrapper()
    this.createWaypoint()
  }

  /* Private */
  Sticky.prototype.createWaypoint = function() {
    var originalHandler = this.options.handler

    this.waypoint = new Waypoint($.extend({}, this.options, {
      element: this.wrapper,
      handler: $.proxy(function(direction) {
        var shouldBeStuck = this.options.direction.indexOf(direction) > -1
        var wrapperHeight = shouldBeStuck ? this.$element.outerHeight(true) : ''

        this.$wrapper.height(wrapperHeight)
        this.$element.toggleClass(this.options.stuckClass, shouldBeStuck)

        if (originalHandler) {
          originalHandler.call(this, direction)
        }
      }, this)
    }))
  }

  /* Private */
  Sticky.prototype.createWrapper = function() {
    if (this.options.wrapper) {
      this.$element.wrap(this.options.wrapper)
    }
    this.$wrapper = this.$element.parent()
    this.wrapper = this.$wrapper[0]
  }

  /* Public */
  Sticky.prototype.destroy = function() {
    if (this.$element.parent()[0] === this.wrapper) {
      this.waypoint.destroy()
      this.$element.removeClass(this.options.stuckClass)
      if (this.options.wrapper) {
        this.$element.unwrap()
      }
    }
  }

  Sticky.defaults = {
    wrapper: '<div class="sticky-wrapper" />',
    stuckClass: 'stuck',
    direction: 'down right'
  }

  Waypoint.Sticky = Sticky
}())

$( document ).ready(function() {
	//Call Waypoints Sticky
        if ($('.router-nav').length > 0) {
            var sticky = new Waypoint.Sticky({
              element: $('.router-nav')[0]
            })
        }
});
///////////////////////Nav Offcanvas///////////////////////
$('.trigger').click(function(e) {
    e.preventDefault();
  var $this = $(this),
  notThis = $this.hasClass('open'),
  thisNav = $this.attr("rel");
  //close if you click another menu trigger
  if (!notThis) {
    $('.triggered-area, .trigger').removeClass('open');
    if ( $( '.trigger' ).hasClass( "block-scroll" ) ){
      $('html').removeClass('disable-scroll');
      $('#overlay-mobile').removeClass('visible');
    }
  }
  //open the nav
  // $this.toggleClass('open');
  $("a[rel="+thisNav+"]").toggleClass('open');
  $("#"+thisNav).toggleClass('open');
  if ( $( this ).hasClass( "block-scroll" ) ){
    $('html').toggleClass('disable-scroll');
    $('#overlay-mobile').toggleClass('visible');
  }
});
//close if you click on anything but this nav item or a trigger
$(document).on('click', function(event) {
  if (!$(event.target).closest('.triggered-area, .trigger').length) {
    $('.triggered-area, .trigger').removeClass('open');
    if ( $( '.trigger' ).hasClass( "block-scroll" ) ){
      $('html').removeClass('disable-scroll');
      $('#overlay-mobile').removeClass('visible');
    }
  }
});

//Close button
$('.offcanvas-close-button').click(function() {
  $('.triggered-area, .trigger').removeClass('open');
  $('html').removeClass('disable-scroll');
  $('#overlay-mobile').removeClass('visible');
});

//Focus on Search input when open
$('.nav-search.trigger').click(function() {
  $( "#page-top-search" ).click();
});
$('.grid-cascade').jaliswall({item:'article', onChange:onChange});
///////////////////////Tabs///////////////////////
// http://www.entheosweb.com/tutorials/css/tabs.asp

//Progressive Enhancement bits:
$("document").ready(function(){
  $(".tab-content").hide();
  $(".tab-content:first").show();
});
//The main bits
$("ul.tabs li").click(function() {
  $(".tab-content").hide();
  var activeTab = $(this).attr("rel");
  $("#"+activeTab).fadeIn();

  $("ul.tabs li").removeClass("active");
  $(this).addClass("active");
});
/////////Accordion///////
$(document).ready(function() {
      $('body').addClass('js');
      var $accordion = $('.accordion');

    $accordion.on("click", function(e){
      e.preventDefault();
      var $this = $(this);
      $this.toggleClass('active');
      $this.next('.panel').toggleClass('active');
    });
});
//////// Slick Carousel /////////

$(document).ready( function() {
  $('#stories-sub-carousel').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow:'.stories-button--previous',
    nextArrow:'.stories-button--next',
    responsive: [
      {
        breakpoint: 919,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});


$(document).ready( function() {
  $('#home-carousel').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: false,
    prevArrow:'.home-button--previous',
    nextArrow:'.home-button--next',
    responsive: [
      {
        breakpoint: 600,
        settings: {
          adaptiveHeight: true
        }
      }
    ]
  });
});


$(document).ready( function() {
  $('.carousel').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    prevArrow:'.carousel-button--previous',
    nextArrow:'.carousel-button--next'
  });
});
//Scroll to top link
var $toplink = $('.back-to-top');
$toplink.click(function() {
    $('html, body').animate({
        scrollTop: $('body').offset().top
    }, 500);
});
$(document).ready(function() {
  $(".sticky").stick_in_parent({
    offset_top: 27
  });
});


$(document).ready(function() {
  $(".sticky-nav").stick_in_parent({
    offset_top: 0
  });
});
$(function () { objectFitImages() });
/////////////////////// Advertistement (5 seconds on screen) ///////////////////////
$(document).ready(function() {
  var $timedAd = $('.ad-timed-bottom');
    $timedAd.addClass('visible');
     setTimeout(function(){
         $timedAd.addClass('close-button-revealed');
    }, 5000);

    $('.ad-close-button').click(function(){
      $timedAd.addClass('hidden');
      $('footer#footer').addClass('ad-hidden');
    });
  });


// If the window width is less than the size that will show the right column, move
// the ad into the body of the page.
if ($(window).width() < 1200) {
	$ad = $('#ONSvoiceROS-300').clone();
	$ad.removeClass('sticky');
	$placeafter = $($('.article-body-content').children('div:not([class*=inbody-img]),p')[4]);
	$placeafter.after($ad);
}
;
