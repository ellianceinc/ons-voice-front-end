== Package outline ==

* bower.json
** File controls installing the necessary bower packages.
* bower_components
** Contains some packages built using bower.
* assets
** images
** scripts
*** bottom.js - The main javacsript file.
*** jquery.cycle2.* -- Controls the carousel on some internal pages.
*** vendor/*  -- 3rd party javascript libraries used in this site.
** stylsheets
*** style.css -- The new CSS file. Controls the header, footer, and homepage.