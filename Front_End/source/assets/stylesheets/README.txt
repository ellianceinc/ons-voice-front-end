## Stylesheet Organization

The styles are compiled using SASS, SCSS to be specific. This is a built in function of Middleman.

##The Folder Strucure
- Each folder has an 'index.scss' file that combines the folders SASS files together, making it easier than dealing with one long, giant page to compile the SASS files.

###01-config
- This is where you should start a project, set up all variables like grid, typography, colors, and breakpoints.

###02-base
- In theory these styles should never be touched. These files include mixins and templates that were set up in 01-config.

###03-utilities
- Reusable chunks of SASS like mixins and reusable classes.

###04-custom
- This folder is where all styles are made and altered. 
- plugins folder contains 3rd party styles, mostly for JavaScript modules like carousels or share embeds

###style.css.scss
- All folder 'index.scss' files are combined here to make the final 'style.css' file. 