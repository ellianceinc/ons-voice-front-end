## Install ##

    bower install git@bitbucket.org:ellianceinc/elliance-tracking-js.git

## Use ##

Add this after the Google Analytics snippet and the JQuery include:

    <script src="/bower_components/elliance-tracking-js/elliance.tracking.js"></script>
    
### Development ###

#### Unit Tests ####

	# Run Karma:
	$ ./node_modules/karma/bin/karma start  my.conf.js

OR

	$ npm install -g karma-cli
	$ karma start  my.conf.js