# Elliance Middleman Base Project

##How to:
- Pull Repro
- Copy or Cut "Front_End" folder and place it into your new project folder
- Follow Middleman instructions inside of "Front_End"
- Have a coffee or beer to celebrate...you're a great developer! 👊


##To Do List:
- Add SASS organization documentation
- Switch Carousels to Flickity
- Remove Salvator Grid from Homepage 